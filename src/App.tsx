import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  Basics,
  CurvedLineChart,
  AxesAndScales,
  AnimatedBarChart,
} from "./lessons";
import "./App.css";

// import config from "./data/config.json";

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Basics</Link>
            </li>
            <li>
              <Link to="/curved-line-chart">Curved Line Chart</Link>
            </li>
            <li>
              <Link to="/axes-and-scales">Axes and Scales</Link>
            </li>
            <li>
              <Link to="/animated-bar-chart">Animated Bar Chart</Link>
            </li>
          </ul>
        </nav>

        <hr />

        <Switch>
          <Route path="/curved-line-chart">
            <CurvedLineChart />
          </Route>
          <Route path="/axes-and-scales">
            <AxesAndScales />
          </Route>
          <Route path="/animated-bar-chart">
            <AnimatedBarChart />
          </Route>
          <Route path="/">
            <Basics />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
