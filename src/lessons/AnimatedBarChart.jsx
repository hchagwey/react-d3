import React, { useRef, useEffect, useState } from "react";
import { select, axisBottom, axisRight, scaleLinear, scaleBand } from "d3";
import styled from "styled-components";

const initalData = [25, 30, 0, 60, 20, 65, 75];

function AnimatedBarChart() {
  const [data, setData] = useState(initalData);
  const svgRef = useRef();

  useEffect(() => {
    const svg = select(svgRef.current);
    const xScale = scaleBand()
      .domain(data.map((value, index) => index))
      .range([0, 300])
      .padding(0.5);

    const yScale = scaleLinear().domain([0, 150]).range([150, 0]);

    const xAxis = axisBottom(xScale).ticks(data.length);
    svg.select(".x-axis").style("transform", "translateY(150px)").call(xAxis);

    const yAxis = axisRight(yScale);
    svg.select(".y-axis").style("transform", "translateX(300px)").call(yAxis);

    svg
      .selectAll(".bar")
      .data(data)
      .join("rect")
      .attr("class", "bar")
      .attr("x", (value, index) => xScale(index))
      .attr("width", xScale.bandwidth())
      .attr("height", 50);
  }, [data]);

  return (
    <React.Fragment>
      <Svg ref={svgRef}>
        <g className="x-axis" />
        <g className="y-axis" />
      </Svg>
      <br />
      <br />
      <br />
      <button onClick={() => setData(initalData)}>Reset data</button>
      <button onClick={() => setData(data.map((value) => value + 5))}>
        Update data
      </button>
      <button onClick={() => setData(data.filter((value) => value <= 35))}>
        Filter data
      </button>
    </React.Fragment>
  );
}

const Svg = styled.svg`
  background: #eee;
  overflow: visible;
  margin-bottom: 2rem;
  display: block;
`;

export default AnimatedBarChart;
